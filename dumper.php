<?php
/**
 * pxls.space image dumper
 * @author Max Beb�k
 */
 
//################// SETTINGS //################//
$url      = "http://pxls.space/boarddata";
$interval = empty($argv[1]) ? 10 : $argv[1]; // in seconds

$imageSX  = 1000;
$imageSY  = 1000;
$savePath = "img";
$imageDateFormat = "Y-m-d_H-i-s";

$colorCodes = [
    [255, 255, 255], // white
    [228, 228, 228], // light grey
    [136, 136, 136], // dark grey
    [ 34,  34,  34], // black
    
    [255, 167, 209], // pink
    [229,   0,   0], // red
    [229, 149,   0], // orange
    [160, 106,  66], // brown
    [229, 217,   0], // yellow
    [148, 224,  68], // ligth green
    [  2, 190,   1], // green
    
    [  0, 211, 221], // cyan
    [  0, 131, 199], // light blue
    [  0,   0, 234], // blue
    [207, 110, 228], // dark pink
    [130,   0, 128]  // purple
];
//##############################################//

echo "=========================\n";
echo "==  pxls.space dumper  ==\n";
echo "==     by Max Beb�k    ==\n";
echo "=========================\n";

for(;;)
{
    try{
        echo "try to take picture...\n";
        
        // connect and recieve data
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL            => $url,
            CURLOPT_USERAGENT      => 'Google Ultron/42.0 (Gentoo 3.1; rv:11.0) Lynx-Engine/2398456934'
        ));
        $data = curl_exec($curl);
        
        if(strlen($data) != ($imageSX * $imageSY))
        {
            echo "Error: wrong result size!\n";
            continue;
        }
        
        curl_close($curl);

        // create output image
        $img = imageCreateTrueColor($imageSX, $imageSY);

        // register all colors
        $palette = [];
        foreach($colorCodes as $c)
        {
            $palette[] = imagecolorallocate($img, $c[0], $c[1], $c[2]);
        }

        // set all pixels
        $i = 0;
        for($y=0; $y<$imageSY; ++$y)
        {
            for($x=0; $x<$imageSX; ++$x)
            {
                $code = ord($data[$i++]);
                if($code > 15)$code = 0;
                
                imagesetpixel($img, $x, $y, $palette[$code]);
            }
        }
        
        // get output...
        ob_start();
        imagepng($img);
        $image_data = ob_get_contents();
        ob_end_clean();

        imagedestroy($img);

        // ... and save it
        $filePath = $savePath."/pxls_".date($imageDateFormat).".png";
        file_put_contents($filePath, $image_data);
        $fileSize = number_format(filesize($filePath) / 1024, 4, ".", "");
        
        echo "Picture taken @ '$filePath', size: {$fileSize}KB\n";
        
    }catch(Exception $e)
    {
        echo "Exception:\n";
        echo $e->getMessage()."\n";
    }
    
    // sleep before taking next picture
    sleep($interval);
}