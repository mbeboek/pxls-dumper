# PXLS-Space dumper
Dumps periodically PNG images of pxls.space.

To start it, run:
```sh
php dumper.php [time]
```
where time is the interval in seconds, which is by default 10.

More settings can be chaged inside the php file.


If you're using the default settings, 

create a directory called "img" in the same location as the script.

Links
----
- [pxls.space](http://pxls.space/)
- [Subreddit](https://www.reddit.com/r/Pxlsspace/)

License
----
Do what the fuck you want to do with it, but im not responsible for anything.

Hail to the void!

Author: Max Beb�k - 2017